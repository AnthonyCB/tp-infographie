#pragma once

#include "ofMain.h"
#include "ofxGui.h"

#include "renderer.h"
#include "rayTracingRender.h"
#include <iostream>
#include <math.h>
#include "ofxAssimpModelLoader.h"
#include <stdio.h>
#include <ctype.h>
#include "camera.h"

enum Filter {nearest, bilinear, trilinear, identity, sharpen, blur};
enum Tool { point, line, square, rectangle,	triangle, quadrilateral, regularPolygon, irregularPolygon, cercle, ellipse, arc, sphere, cube, curve};
enum Light {ambiante, directionnelle, ponctuelle, projecteur};
enum Switch {on,off};

class ofApp : public ofBaseApp
{
	Renderer renderer;
	Camera camera;

	//Cameras
	bool is_key_press_up;
	bool is_key_press_down;
	bool is_key_press_left;
	bool is_key_press_right;
	bool is_key_press_w;
	bool is_key_press_s;
	bool is_key_press_plus;
	bool is_key_press_minus;

	ofxGuiGroup group_camera;
	ofParameter<int> cameraId;
	ofParameter<string> cameraName;
	ofParameter<bool> perspective;

	RayTracingRender rayTracingRenderer;
	Switch index;
	ofxPanel gui;

	ofxGuiGroup group_draw;

	std::vector<ofImage> importedImages;

	ofParameter<ofColor> color_picker_background;
	ofParameter<int> background_hue;
	ofParameter<int> background_saturation;
	ofParameter<int> background_brightness;
	ofParameter<ofColor> color_picker_stroke;
	ofParameter<int> stroke_hue;
	ofParameter<int> stroke_saturation;
	ofParameter<int> stroke_brightness;
	ofParameter<ofColor> color_picker_fill;
	ofParameter<int> fill_hue;
	ofParameter<int> fill_saturation;
	ofParameter<int> fill_brightness;

	ofParameter<float> slider_stroke_weight;

	ofxGuiGroup group_tools;

	ofParameter<bool> checkbox;

	std::vector<ofParameter<bool>> toolCheckboxes;
	std::vector<ofParameter<bool>> lightCheckboxes;
	std::vector<ofParameter<bool>> lightOnOffbox;

	ofxGuiGroup group_filtering;
	ofxButton f_identityButton;
	ofxButton f_sharpenButton;
	ofxButton f_blurButton;
	ofxButton f_embossButton;
	ofxButton f_edgeDetectButton;

	ofxGuiGroup groupe_mapping;
	ofParameter<float> slider_exposure;

	std::vector<ofPath> mouseIcons;

	ofParameter<bool> boundingBoxCheckbox;

	ofParameter<bool> shaderTextureCheckbox;
	ofxGuiGroup group_texture;

	ofxGuiGroup group_light;

	ofxButton exportButton;

	Tool currentTool;
	Light currentLight;
	Switch currentSwitch;

	vector<ofPoint> drawingSteps;

	ofParameter<int> slider_sides_regular_poly;
	ofParameter<int> slider_sides_light_intensity;

	ofxGuiGroup rayTracing_tools;
	ofParameter<bool> rayTracingTooltip;
	ofParameter<ofColor> rayColor1;
	ofParameter<ofColor> rayColor2;
	ofParameter<float>     rayRadius1;
	ofParameter<float>     rayRadius2;
	ofParameter<float>     rayIntLight;
	ofParameter<ofVec2f>   raySpeLight;
	ofParameter<ofColor>   rayColLight;
	ofParameter<float>     rayAmbient;
	ofParameter<ofVec3f>   rayPosSphere1;
	ofParameter<ofVec3f>   rayPosSphere2;
	int currentCurvePoint;

	void setup();
	void update();
	void draw();
	void exit();

	void keyPressed(int key);
	void keyReleased(int key);

	void mousePressed(int x, int y, int button);		

	void windowResized(int w, int h);

	void dragEvent(ofDragInfo dragInfo);

	void projectionChanged(bool& value);
	void toolChanged(bool& value);
	void lightChanged(bool& value);
	void switchChanged(bool& value);
	void onCameraChanged(int& value);

	void drawTempPrimitives();

	void setMouseIcons();

	void f_identityPressed();
	void f_sharpenPressed();
	void f_blurPressed();
	void f_embossPressed();
	void f_edgeDetectPressed();
	void exporter();

	void onBackgroundRGBColorChanged(ofColor& color);
	void onBackgroundHueChanged(int &value);
	void onBackgroundSaturationChanged(int &value);
	void onBackgroundBrightnessChanged(int &value);

	void onStrokeRGBColorChanged(ofColor& color);
	void onStrokeHueChanged(int &value);
	void onStrokeSaturationChanged(int &value);
	void onStrokeBrightnessChanged(int &value);

	void onFillRGBColorChanged(ofColor& color);
	void onFillHueChanged(int &value);
	void onFillSaturationChanged(int &value);
	void onFillBrightnessChanged(int &value);
	glm::vec3 getMousePosition();
};