#pragma once

#include "ofMain.h"
#include "algorithm"
#include <iostream>
#include "ofxAssimpModelLoader.h"
#include "ofxBezierSurface.h"

// �num�ration des types de shader
enum class ShaderType { color_fill, lambert, gouraud, phong, blinn_phong };
enum shaderDynamicLight { pointLights1Light_position, pointLights2Light_position, pointLights3Light_position, pointLights4Light_position};
//enum Light { ambiante, directionnelle, ponctuelle, projecteur };

//�num types de kernel de convolution
enum class ConvolutionKernel {
	identity,
	sharpen,
	blur,
	emboss,
	edge_detect
};

//kernel identity
const std::array<float, 9> convolution_kernel_identity
{
  0.0f,  0.0f,  0.0f,
  0.0f,  1.0f,  0.0f,
  0.0f,  0.0f,  0.0f
};

//kernel sharpen
const std::array<float, 9> convolution_kernel_sharpen
{
	0.0f, -1.0f,  0.0f,
	-1.0f,  5.0f, -1.0f,
	0.0f, -1.0f,  0.0f
};

//kernel blur
const std::array<float, 9> convolution_kernel_blur
{
  1.0f / 9.0f,  1.0f / 9.0f,  1.0f / 9.0f,
  1.0f / 9.0f,  1.0f / 9.0f,  1.0f / 9.0f,
  1.0f / 9.0f,  1.0f / 9.0f,  1.0f / 9.0f
};

//kernel emboss
const std::array<float, 9> convolution_kernel_emboss
{
 -2.0f, -1.0f,  0.0f,
 -1.0f,  1.0f,  1.0f,
  0.0f,  1.0f,  2.0f
};

//kernel edge detector
const std::array<float, 9> convolution_kernel_edge_detect
{
  0.0f,  1.0f,  0.0f,
  1.0f, -4.0f,  1.0f,
  0.0f,  1.0f,  0.0f
};

class Renderer
{
public:

	ofImage image;
	ofShader shaderExposure;
	float exposureFactor;
	ofVec3f position_cube;
	float scale_cube;
	ofShader shaderTexture;
	bool addTexture = false;
	bool isLightActive = false;

	ofTrueTypeFont font;

	ofPath mouseIcon;

	ofColor background_color;

	ofColor stroke_color;

	ofColor fill_color;

	ofRectangle bounding_box;

	std::vector<ofPath> shapes;

	std::vector<of3dPrimitive> shapes3D;

	std::vector<ofxAssimpModelLoader> models;

	std::vector<of3dPrimitive> boundingBoxesShapes3D;

	std::vector<of3dPrimitive> boundingBoxesModels;

	std::vector<ofPolyline> curves;

	std::vector<ofLight> lights;

	ofxBezierSurface surface;

	ofColor light_ambient;
	ofLight light_directional;
	ofLight light_point;
	ofLight light_spot;

	bool is_active_ligh_ambient;
	bool is_active_light_directional;
	bool is_active_light_point;
	bool is_active_light_spot;

	float camera_offset;

	ofQuaternion orientation_directional;
	ofQuaternion orientation_spot;

	void lighting_on();
	void lighting_off();

	string text;

	float stroke_weight;

	float line_offset;

	int font_size;

	void setup();
	void update();
	void draw();

	void drawBoundingBoxes();

	void image_export(const string name, const string extension) const;

	void setImageRatio(float ratio);
	float getImageRatio();

	//Texture
	void filtrageConvolution(ConvolutionKernel kernelType);

	//Shader 
	shaderDynamicLight index;
	ShaderType shader_active;

	ofShader shader_color_fill;
	ofShader shader_lambert;
	ofShader shader_gouraud;
	ofShader shader_phong;
	ofShader shader_blinn_phong;
	ofShader* shader;

	ofLight light;
	ofLight dynamicLight1;
	ofLight dynamicLight2;
	ofLight dynamicLight3;
	ofLight dynamicLight4;
	string shader_name;
	float oscillation;
	float oscillation_frequency;
	float oscillation_amplitude;

	float speed_motion;

	float offset_x;
	float offset_z;

	float delta_x;
	float delta_z;

	float initial_x;
	float initial_z;

	float center_x;
	float center_y;
	float oscillate(float time, float frequency, float amplitude);
	void setDynamicLight();

private:
	float imageRatio = 1.0f;

	float imageHeight;
	float imageWidth;
	float posX;
	float posY;

	void initializeSurface();
};
