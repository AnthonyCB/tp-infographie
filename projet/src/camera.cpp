#include "camera.h"

void Camera::setup()
{

	camera_position = { 0.0f, 0.0f, 0.0f };
	camera_target = { 0.0f, 0.0f, 0.0f };
	camera_near = 50.0f;
	camera_far = 1750.0f;
	camera_fov = 60.0f;
	camera_fov_delta = 16.0f;
	speed_delta = 250.0f;

	ofCamera* camera_front = new ofCamera();
	ofCamera* camera_back = new ofCamera();
	ofCamera* camera_left = new ofCamera();
	ofCamera* camera_right = new ofCamera();
	ofCamera* camera_top = new ofCamera();
	ofCamera* camera_bottom = new ofCamera();

	cameras.push_back(camera_front);
	cameras.push_back(camera_back);
	cameras.push_back(camera_left);
	cameras.push_back(camera_right);
	cameras.push_back(camera_top);
	cameras.push_back(camera_bottom);

	camera = cameras.at(0);
	camera_active = TypeCamera::front;

	is_camera_move_left = false;
	is_camera_move_right = false;
	is_camera_move_up = false;
	is_camera_move_down = false;
	is_camera_move_forward = false;
	is_camera_move_backward = false;
	is_camera_perspective = true;
	is_camera_fov_narrow = false;
	is_camera_fov_wide = false;

	reset();
	setPerspective();
}

void Camera::reset()
{

	offset_scene = -200.0f;
	offset_camera = offset_scene * 3.5f * -1.0f;

	switch (camera_active) {
	case TypeCamera::front:
		camera->setPosition(ofVec3f(0, 0, offset_camera));
		camera->setOrientation(ofVec3f(0,0,-1));
		break;

	case TypeCamera::back:
		camera->setPosition(ofVec3f(0, 0, -offset_camera));
		camera->lookAt(camera_target);
		break;

	case TypeCamera::left:
		camera->setPosition(ofVec3f(-offset_camera, 0, 0));
		camera->lookAt(camera_target);
		break;

	case TypeCamera::right:
		camera->setPosition(ofVec3f(offset_camera, 0, 0));
		camera->lookAt(camera_target);
		break;

	case TypeCamera::top:
		camera->setPosition(ofVec3f(0, offset_camera, 0));
		camera->lookAt(camera_target, ofVec3f(1, 0, 0));
		break;

	case TypeCamera::bottom:
		camera->setPosition(ofVec3f(0, -offset_camera, 0));
		camera->lookAt(camera_target, ofVec3f(1, 0, 0));
		break;
	}
}

void Camera::update()
{

	time_current = ofGetElapsedTimef();
	time_elapsed = time_current - time_last;
	time_last = time_current;

	speed_translation = speed_delta * time_elapsed;
	speed_rotation = speed_translation / 8.0f;

	if (is_camera_move_left)
		camera->truck(-speed_translation);
	if (is_camera_move_right)
		camera->truck(speed_translation);

	if (is_camera_move_up)
		camera->boom(speed_translation);
	if (is_camera_move_down)
		camera->boom(-speed_translation);

	if (is_camera_move_forward)
		camera->dolly(-speed_translation);
	if (is_camera_move_backward)
		camera->dolly(speed_translation);

	if (is_camera_perspective)
	{ 
		if (is_camera_fov_narrow)
		{
			camera_fov = std::max(camera_fov -= camera_fov_delta * time_elapsed, 0.0f);
			camera->setFov(camera_fov);
		}

		if (is_camera_fov_wide)
		{
			camera_fov = std::min(camera_fov += camera_fov_delta * time_elapsed, 180.0f);
			camera->setFov(camera_fov);
		}
	}
}

void Camera::setPerspective()
{
	camera_position = camera->getPosition();
	camera_orientation = camera->getOrientationQuat();

	if (is_camera_perspective)
	{
		camera->disableOrtho();
		camera->setupPerspective(false, camera_fov, camera_near, camera_far, ofVec2f(0, 0));
	}
	else
	{
		camera->enableOrtho();
	}

	camera->setPosition(camera_position);
	camera->setOrientation(camera_orientation);
}



