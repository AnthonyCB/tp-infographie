#include "ofMain.h"
#include "ofApp.h"

int main()
{
	ofGLFWWindowSettings windowSettings;
	windowSettings.resizable = true;
	windowSettings.setSize(1024, 512);
	windowSettings.setGLVersion(3, 3);
	windowSettings.numSamples = 4;
	windowSettings.windowMode = ofWindowMode::OF_WINDOW;
	windowSettings.setSize(800, 600);
	ofCreateWindow(windowSettings);
	ofRunApp(new ofApp());
}
