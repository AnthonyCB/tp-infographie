#include "rayTracingRender.h"

void RayTracingRender::setup() {
	ofSetFrameRate(60);
	raytracingShader.load("raytracing_330_vs.glsl","raytracing_330_fs.glsl");
	w = ofGetScreenWidth();
	h = ofGetScreenHeight();
	fbo.allocate(w, h);
	fbo.begin();
	ofClear(0, 0, 0, 0);
	fbo.end();
}

void RayTracingRender::update() {

}

void RayTracingRender::draw() {
	ofBackgroundGradient(225, 0);

	raytracingShader.begin();
	raytracingShader.setUniform1f("u_aspect_ratio", w / h);
	raytracingShader.setUniform3f("sphere_position_0", posSphere1.x, posSphere1.y, posSphere1.z);
	raytracingShader.setUniform1f("sphere_radius_0", radius1);
	raytracingShader.setUniform3f("sphere_color_0", color1.x, color1.y, color1.z);

	raytracingShader.setUniform3f("sphere_position_1", posSphere2.x, posSphere2.y, posSphere2.z);
	raytracingShader.setUniform1f("sphere_radius_1", radius2);
	raytracingShader.setUniform3f("sphere_color_1", color2.x, color2.y, color2.z);

	raytracingShader.setUniform3f("plane_position", 0., -.5, 0.);
	raytracingShader.setUniform3f("plane_normal", 0., 1., 0.043);

	raytracingShader.setUniform1f("light_intensity", intensity);
	raytracingShader.setUniform2f("light_specular", speLight.x, speLight.y);
	raytracingShader.setUniform3f("light_position", 5., 5., -5.);
	raytracingShader.setUniform3f("light_color", colorLight.x, colorLight.y, colorLight.z);
	raytracingShader.setUniform1f("ambient", ambient);
	raytracingShader.setUniform3f("O", 0., 0., -1.);

	fbo.draw(-100, -100);

	raytracingShader.end();
}