#pragma once

#include "ofMain.h"
#include "algorithm"
#include <iostream>
#include "ofxAssimpModelLoader.h"
#include "vector";

enum TypeCamera {
	front,
	back,
	left,
	right,
	top,
	bottom
};

class Camera
{
public:

	vector<ofCamera*> cameras;

	TypeCamera camera_active;
	ofCamera* camera;
	string camera_name;
	string camera_projection;

	ofVec3f camera_position;
	ofVec3f camera_target;
	glm::quat camera_orientation;

	float camera_near;
	float camera_far;

	float camera_fov;
	float camera_fov_delta;

	float offset_camera;
	float offset_color;
	float offset_scene;
	float offset_cube;

	float speed_delta;
	float speed_translation;
	float speed_rotation;

	float time_current;
	float time_last;
	float time_elapsed;
	bool is_camera_move_left;
	bool is_camera_move_right;
	bool is_camera_move_up;
	bool is_camera_move_down;
	bool is_camera_move_forward;
	bool is_camera_move_backward;
	bool is_camera_perspective;
	bool is_camera_fov_narrow;
	bool is_camera_fov_wide;
	void reset();
	void setup();
	void update();
	void setPerspective();



};
