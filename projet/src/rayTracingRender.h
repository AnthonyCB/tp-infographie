#pragma once

#include "ofMain.h"
#include "algorithm"
#include <iostream>
#include "ofxAssimpModelLoader.h"

class RayTracingRender
{
public:
	ofVec3f color1;
	ofVec3f color2;
	float radius1;
	float radius2;
	ofVec2f speLight;
	ofVec3f colorLight;
	float ambient;
	float intensity;
	ofVec3f posSphere1;
	ofVec3f posSphere2;

	ofFbo fbo;
	int w, h;
	ofShader raytracingShader;

	void setup();
	void update();
	void draw();
};

