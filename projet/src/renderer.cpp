#include "renderer.h"

void Renderer::setup()
{
	
	ofSetFrameRate(60);
	ofSetBackgroundColor(31);
	ofDisableArbTex();


	font_size = 64;
	scale_cube = 100.0f;
	// param�tres
	oscillation_amplitude = 32.0f;
	oscillation_frequency = 7500.0f;
	speed_motion = 150.0f;
	initial_z = -100.0f;
	// initialisation des variables
	offset_x = initial_x;
	offset_z = initial_z;

	delta_x = speed_motion;
	delta_z = speed_motion;
	// charger, compiler et linker les sources des shaders
	shader_color_fill.load(
		"shader/color_fill_330_vs.glsl",
		"shader/color_fill_330_fs.glsl");

	shader_lambert.load(
		"shader/lambert_330_vs.glsl",
		"shader/lambert_330_fs.glsl");

	shader_gouraud.load(
		"shader/gouraud_330_vs.glsl",
		"shader/gouraud_330_fs.glsl");

	shader_phong.load(
		"shader/phong_330_vs.glsl",
		"shader/phong_330_fs.glsl");

	shader_blinn_phong.load(
		"shader/blinn_phong_330_vs.glsl",
		"shader/blinn_phong_330_fs.glsl");

	// shader actif au lancement de la sc�ne
	shader_active = ShaderType::blinn_phong;

	//line_offset = font_size / 2.0f;

	font.load("CONSOLA.TTF", font_size);

	exposureFactor = 1.0f;
	light_ambient.set(127, 127, 127);
	lights.push_back(dynamicLight1);
	lights.push_back(dynamicLight2);
	lights.push_back(dynamicLight3);
	lights.push_back(dynamicLight4);
	shaderExposure.load("exposure_330_vs.glsl", "exposure_330_fs.glsl");
	shaderTexture.load("texture_330_vs.glsl", "texture_330_fs.glsl");
	boundingBoxesShapes3D = vector<of3dPrimitive>();

	boundingBoxesModels = vector<of3dPrimitive>();

	//Type de lumieres
	  // configurer la lumi�re ambiante
	light_ambient.set(127, 127, 127);

	// configurer la lumi�re directionnelle
	light_directional.setDiffuseColor(ofColor(31, 255, 31));
	light_directional.setSpecularColor(ofColor(191, 191, 191));
	light_directional.setOrientation(ofVec3f(0.0f, 0.0f, 0.0f));
	light_directional.setDirectional();

	// configurer la lumi�re ponctuelle
	light_point.setDiffuseColor(ofColor(255, 255, 255));
	light_point.setSpecularColor(ofColor(191, 191, 191));
	light_point.setPointLight();

	// configurer la lumi�re projecteur
	light_spot.setDiffuseColor(ofColor(191, 191, 191));
	light_spot.setSpecularColor(ofColor(191, 191, 191));
	light_spot.setOrientation(ofVec3f(0.0f, 0.0f, 0.0f));
	light_spot.setSpotConcentration(2);
	light_spot.setSpotlightCutOff(30);
	light_spot.setSpotlight();

	initializeSurface();
}

void Renderer::update()
{
	// transformer la lumi�re
	if (isLightActive) {
		light.setGlobalPosition(
			ofMap(ofGetMouseX() / (float)ofGetWidth(), 0.0f, 1.0f, -ofGetWidth() / 2.0f, ofGetWidth() / 2.0f),
			ofMap(ofGetMouseY() / (float)ofGetHeight(), 0.0f, 1.0f, -ofGetHeight() / 2.0f, ofGetHeight() / 2.0f),
			-offset_z * 1.5f);

		ofPushMatrix();
		if (is_active_light_directional)
		{
			// transformer la lumi�re directionnelle
			orientation_directional.makeRotate(int(ofGetElapsedTimeMillis() * 0.1f) % 360, 0, 1, 0);

			light_directional.setPosition(center_x, center_y + 60, camera_offset * 0.75f);
			light_directional.setOrientation(orientation_directional);
		}

		if (is_active_light_point)
		{
			// transformer la lumi�re ponctuelle
			light_point.setPosition(ofGetMouseX(), ofGetMouseY(), camera_offset * 0.75f);
		}

		if (is_active_light_spot)
		{
			// transformer la lumi�re projecteur
			oscillation = oscillate(ofGetElapsedTimeMillis(), oscillation_frequency, oscillation_amplitude);

			orientation_spot.makeRotate(30.0f, ofVec3f(1, 0, 0), oscillation, ofVec3f(0, 1, 0), 0.0f, ofVec3f(0, 0, 1));

			light_spot.setOrientation(orientation_spot);

			light_spot.setPosition(center_x, center_y - 75.0f, camera_offset * 0.75f);
		}
		ofPopMatrix();
	}


	// mise � jour d'une valeur num�rique anim�e par un oscillateur
	float oscillation = oscillate(ofGetElapsedTimeMillis(), oscillation_frequency, oscillation_amplitude) + oscillation_amplitude;

	// passer les attributs uniformes au shader de sommets
	switch (shader_active)
	{
	case ShaderType::color_fill:
		shader_name = "Color Fill";
		shader = &shader_color_fill;
		shader->begin();
		shader->setUniform3f("color", 0.6f, 0.6f, 0.6f);
		shader->end();
		break;

	case ShaderType::lambert:
		shader_name = "Lambert";
		shader = &shader_lambert;
		shader->begin();
		shader->setUniform3f("color_ambient", 0.1f, 0.1f, 0.1f);
		shader->setUniform3f("color_diffuse", 0.6f, 0.6f, 0.6f);
		shader->setUniform3f("light_position", glm::vec4(light.getGlobalPosition(), 0.0f) * ofGetCurrentMatrix(OF_MATRIX_MODELVIEW));
		shader->end();
		break;

	case ShaderType::gouraud:
		shader_name = "Gouraud";
		shader = &shader_gouraud;
		shader->begin();
		shader->setUniform3f("color_ambient", 0.1f, 0.1f, 0.1f);
		shader->setUniform3f("color_diffuse", 0.6f, 0.6f, 0.6f);
		shader->setUniform3f("color_specular", 1.0f, 1.0f, 0.0f);
		shader->setUniform1f("brightness", oscillation);
		shader->setUniform3f("light_position", glm::vec4(light.getGlobalPosition(), 0.0f) * ofGetCurrentMatrix(OF_MATRIX_MODELVIEW));
		shader->end();
		break;

	case ShaderType::phong:
		shader_name = "Phong";
		shader = &shader_phong;
		shader->begin();
		shader->setUniform3f("color_ambient", 0.1f, 0.1f, 0.1f);
		shader->setUniform3f("color_diffuse", 0.6f, 0.6f, 0.6f);
		shader->setUniform3f("color_specular", 1.0f, 1.0f, 0.0f);
		shader->setUniform1f("brightness", oscillation);
		shader->setUniform3f("light_position", glm::vec4(light.getGlobalPosition(), 0.0f) * ofGetCurrentMatrix(OF_MATRIX_MODELVIEW));
		shader->end();
		break;

	case ShaderType::blinn_phong:
		shader_name = "Blinn-Phong";
		shader = &shader_blinn_phong;
		shader->begin();
		shader->setUniform3f("color_ambient", 0.1f, 0.1f, 0.1f);
		shader->setUniform3f("color_diffuse", 0.6f, 0.6f, 0.6f);
		shader->setUniform3f("color_specular", 1.0f, 1.0f, 0.0f);
		shader->setUniform1f("brightness", oscillation);
		shader->setUniform3f("light_position", glm::vec4(light.getGlobalPosition(), 0.0f) * ofGetCurrentMatrix(OF_MATRIX_MODELVIEW));
		for (int i = 0; i < lights.size(); i++) {
			auto dynamicLightPosition = lights[i].getPosition();
			if (!(dynamicLightPosition.x == 0.000f && dynamicLightPosition.y == 0.000f && dynamicLightPosition.z == 0.000f)) {
				if (i == 0) {
					shader->setUniform3f("color_ambient", 0.1f, 0.1f, 0.1f);
					shader->setUniform3f("color_diffuse", 0.6f, 0.6f, 0.0f);
					shader->setUniform3f("color_specular", 1.0f, 1.0f, 0.0f);
					shader->setUniform1f("brightness", oscillation);
					shader->setUniform3f("pointLights1Light_position", glm::vec4(lights[0].getGlobalPosition(), 0.0f) * ofGetCurrentMatrix(OF_MATRIX_MODELVIEW));
				}else if (i == 1) {
					shader->setUniform3f("color_ambient", 0.1f, 0.1f, 0.1f);
					shader->setUniform3f("color_diffuse", 0.6f, 0.6f, 0.6f);
					shader->setUniform3f("color_specular", 1.0f, 1.0f, 0.0f);
					shader->setUniform1f("brightness", oscillation);
					shader->setUniform3f("light_position", glm::vec4(lights[1].getGlobalPosition(), 0.0f) * ofGetCurrentMatrix(OF_MATRIX_MODELVIEW));
				}else if (i == 2) {
					shader->setUniform3f("color_ambient", 0.1f, 0.1f, 0.1f);
					shader->setUniform3f("color_diffuse", 0.6f, 0.6f, 0.0f);
					shader->setUniform3f("color_specular", 1.0f, 1.0f, 0.0f);
					shader->setUniform1f("brightness", oscillation);
					shader->setUniform3f("light_position", glm::vec4(lights[2].getGlobalPosition(), 0.0f) * ofGetCurrentMatrix(OF_MATRIX_MODELVIEW));
				}else if (i == 3) {
					shader->setUniform3f("color_ambient", 0.1f, 0.1f, 0.1f);
					shader->setUniform3f("color_diffuse", 0.6f, 0.6f, 0.6f);
					shader->setUniform3f("color_specular", 1.0f, 1.0f, 0.0f);
					shader->setUniform1f("brightness", oscillation);
					shader->setUniform3f("light_position", glm::vec4(lights[3].getGlobalPosition(), 0.0f) * ofGetCurrentMatrix(OF_MATRIX_MODELVIEW));
				}
			}
		}
		shader->end();
		break;
	
	default:
		break;
	}

	ofSetColor(stroke_color);
	ofSetLineWidth(stroke_weight);

	bounding_box = font.getStringBoundingBox(text, 0, 0);
	
	surface.update();
	
}

void Renderer::draw()
{
	ofClear(background_color);
	//// activer l'�clairage dynamique
	if (isLightActive) {
		ofPushMatrix();
		// afficher un rep�re visuel pour les lumi�res
		if (is_active_light_point)
			light = light_point;
		if (is_active_light_directional)
			light = light_directional;
		if (is_active_light_spot)
			light = light_spot;
		light.draw();
		ofPopMatrix();
		ofEnableLighting();
		//// activer la lumi�re dynamique
		light.enable();

		for (int i = 0; i < lights.size(); i++) {
			auto dynamicLightPosition = lights[i].getPosition();
			if (!(dynamicLightPosition.x == 0.000f && dynamicLightPosition.y == 0.000f && dynamicLightPosition.z == 0.000f)) {
				lights[i].enable();
			}
		}
	}

	//ofPushMatrix();
	//ofTranslate(center_x + offset_x, center_y, offset_z);

	// rotation locale
	//ofRotateDeg(45.0f, 1.0f, 0.0f, 0.0f);

	font.drawString(
		text,
		(ofGetWidth() / 2.0f) - (bounding_box.getWidth() / 2.0f),
		(ofGetHeight() / 2.0f) + (bounding_box.getHeight() / 2.0f));

	ofDrawLine(
		(ofGetWidth() / 2.0f) - (bounding_box.getWidth() / 2.0f),
		(ofGetHeight() / 2.0f) + (bounding_box.getHeight() / 2.0f) + line_offset,
		(ofGetWidth() / 2.0f) + (bounding_box.getWidth() / 2.0f),
		(ofGetHeight() / 2.0f) + (bounding_box.getHeight() / 2.0f) + line_offset);

		shaderExposure.begin();

		shaderExposure.setUniformTexture("image", image.getTexture(), 1);
		shaderExposure.setUniform1f("factor", exposureFactor);

		image.draw(0, 0);

		shaderExposure.end();


	// activer le shader



	for (int i = 0; i < shapes.size(); i++)
	{
		if (addTexture) {
			ofPushMatrix();
			shaderTexture.begin();
		}
		ofPushMatrix();
		//shader->begin();
		shapes[i].draw();
		//shader->begin();
		ofPopMatrix();
		if (addTexture) {
			ofPopMatrix();
			shaderTexture.end();
		}
	}



	for (int i = 0; i < shapes3D.size(); i++){
	if(isLightActive){
		shader->begin();
		ofPushMatrix();
		shapes3D[i].draw();
		ofPopMatrix();
		shader->end();
	}else {
			ofPushMatrix();
			shapes3D[i].draw();
			ofPopMatrix();
		}
	}

	for (size_t i = 0; i < models.size(); i++)
	{
		//shader->begin();
		ofPushMatrix();
		models[i].drawWireframe();
		ofPopMatrix();
		//shader->begin();
	}

	for (size_t i = 0; i < curves.size(); i++)
	{
		ofPushMatrix();
		curves[i].draw();
		ofPopMatrix();
	}

	// d�sactiver la lumi�re
	light.disable();

	for (int i = 0; i < lights.size() -1; i++) {
		lights[i].disable();
	}

	//// d�sactiver l'�clairage dynamique
	ofDisableLighting();
	mouseIcon.draw();
	surface.drawWireframe();
	surface.drawControls();
}

void Renderer::drawBoundingBoxes() {
	for (int i = 0; i < shapes3D.size(); i++)
	{
		bool doesBoxExist = false;
		int nbBoxes = boundingBoxesShapes3D.size() - 1; //needed else comparasons will be backwards
		if (nbBoxes >= i) {
			if (boundingBoxesShapes3D[i].getMesh().getNumVertices() != 0) {
				doesBoxExist = true;
			}
			else {
				doesBoxExist = false;
			}
		}
		if (doesBoxExist) {
			boundingBoxesShapes3D[i].setPosition(shapes3D[i].getPosition().x, shapes3D[i].getPosition().y, shapes3D[i].getPosition().z);
			boundingBoxesShapes3D[i].drawWireframe();
		}
		else {
			float maxX = std::numeric_limits<float>::min();
			float minX = std::numeric_limits<float>::max();
			float maxY = std::numeric_limits<float>::min();
			float minY = std::numeric_limits<float>::max();
			float maxZ = std::numeric_limits<float>::min();
			float minZ = std::numeric_limits<float>::max();
			for (size_t j = 0; j < shapes3D[i].getMesh().getNumVertices(); j++)
			{
				if (shapes3D[i].getMesh().getVertex(j).x > maxX) {
					maxX = shapes3D[i].getMesh().getVertex(j).x;
				}
				else if (shapes3D[i].getMesh().getVertex(j).x < minX) {
					minX = shapes3D[i].getMesh().getVertex(j).x;
				}
				if (shapes3D[i].getMesh().getVertex(j).y > maxY) {
					maxY = shapes3D[i].getMesh().getVertex(j).y;
				}
				else if (shapes3D[i].getMesh().getVertex(j).y < minY) {
					minY = shapes3D[i].getMesh().getVertex(j).y;
				}
				if (shapes3D[i].getMesh().getVertex(j).z > maxZ) {
					maxZ = shapes3D[i].getMesh().getVertex(j).z;
				}
				else if (shapes3D[i].getMesh().getVertex(j).z < minZ) {
					minZ = shapes3D[i].getMesh().getVertex(j).z;
				}
			}
			ofBoxPrimitive box = ofBoxPrimitive();
			float dimentionX = maxX - minX;
			float dimentionY = maxY - minY;
			float dimentionZ = maxZ - minZ;
			box.set(dimentionX, dimentionY, dimentionZ);
			box.getMesh().addColor(ofDefaultColorType(ofColor(255, 255, 255, 255)));
			box.setPosition(shapes3D[i].getPosition().x, shapes3D[i].getPosition().y, shapes3D[i].getPosition().z);
			box.drawWireframe();
			if (nbBoxes < i) {
				boundingBoxesShapes3D.push_back(box);
			}
			else {
				boundingBoxesShapes3D[i] = box;
			}
		}
	}


	/*for (int i = 0; i < models.size(); i++)
	{
		bool doesBoxExist = false;
		int nbBoxes = boundingBoxesModels.size() - 1; //needed else comparasons will be backwards
		if (nbBoxes >= i) {
			if (boundingBoxesModels[i].getMesh().getNumVertices() != 0) {
				doesBoxExist = true;
			}
			else {
				doesBoxExist = false;
			}
		}
		if (doesBoxExist) {
			boundingBoxesModels[i].setPosition(models[i].getPosition().x, models[i].getPosition().y, models[i].getPosition().z);
			boundingBoxesModels[i].drawWireframe();
		}
		else {
			float maxX = std::numeric_limits<float>::min();
			float minX = std::numeric_limits<float>::max();
			float maxY = std::numeric_limits<float>::min();
			float minY = std::numeric_limits<float>::max();
			float maxZ = std::numeric_limits<float>::min();
			float minZ = std::numeric_limits<float>::max();
			for (size_t j = 0; j < models[i].getNumMeshes(); j++)
			{
				for (size_t k = 0; k < models[i].getMesh(j).getNumVertices(); k++)
				{
					if (models[i].getMesh(j).getVertex(k).x > maxX) {
						maxX = models[i].getMesh(j).getVertex(k).x;
					}
					else if (models[i].getMesh(j).getVertex(k).x < minX) {
						minX = models[i].getMesh(j).getVertex(k).x;
					}
					if (models[i].getMesh(j).getVertex(k).y > maxY) {
						maxY = models[i].getMesh(j).getVertex(k).y;
					}
					else if (models[i].getMesh(j).getVertex(k).y < minY) {
						minY = models[i].getMesh(j).getVertex(k).y;
					}
					if (models[i].getMesh(j).getVertex(k).z > maxZ) {
						maxZ = models[i].getMesh(j).getVertex(k).z;
					}
					else if (models[i].getMesh(j).getVertex(k).z < minZ) {
						minZ = models[i].getMesh(j).getVertex(k).z;
					}
				}
			}
			ofBoxPrimitive box = ofBoxPrimitive();
			float dimentionX = maxX - minX;
			float dimentionY = maxY - minY;
			float dimentionZ = maxZ - minZ;
			box.set(dimentionX, dimentionY, dimentionZ);
			box.getMesh().addColor(ofDefaultColorType(ofColor(0, 230, 64, 1)));
			box.setPosition(models[i].getPosition().x, models[i].getPosition().y, models[i].getPosition().z);
			box.drawWireframe();
			if (nbBoxes < i) {
				boundingBoxesModels.push_back(box);
			}
			else {
				boundingBoxesModels[i] = box;
			}
		}
	}*/
}

void Renderer::image_export(const string name, const string extension) const
{
	ofImage image;
	string time_stamp = ofGetTimestampString("-%y%m%d-%H%M%S-%i");
	string file_name = name + time_stamp + "." + extension;
	image.grabScreen(0, 0, ofGetWindowWidth(), ofGetWindowHeight());
	image.save(file_name);
}

void Renderer::setImageRatio(float ratio) {
	imageRatio = ratio;
}

float Renderer::getImageRatio() {
	return imageRatio;
}

void Renderer::filtrageConvolution(ConvolutionKernel kernelType) {
	ofImage newImage = image;

	int x, y;
	int j, i;
	int c;
	int height = image.getHeight();
	int width = image.getWidth();
	int kernel_index;
	float kernel_value;
	const int kernel_size = 3;
	const int kernel_offset = kernel_size / 2;
	const int color_component_count = 3;

	ofPixels pixel_array = image.getPixels();
	ofPixels pixel_array_new = newImage.getPixels();
	ofColor pixel_color;
	ofColor pixel_color_new;
	int pixel_index;
	int pixel_index_new;
	float sum[color_component_count];

	for (y = 0; y < height; ++y) {
		for (x = 0; x < width; ++x) {
			for (c = 0; c < color_component_count; ++c) {
				sum[c] = 0;
			}
			pixel_index_new = (width * y + x) * color_component_count;

			for (j = -kernel_offset; j <= kernel_offset; ++j) {
				for (i = -kernel_offset; i <= kernel_offset; ++i) {
					pixel_index = (width * (y - j) + (x - i)) * color_component_count;
					pixel_color = pixel_array.getColor(pixel_index);
					kernel_index = kernel_size * (j + kernel_offset) + (i + kernel_offset);

					switch (kernelType) {
					case ConvolutionKernel::sharpen:
						kernel_value = convolution_kernel_sharpen.at(kernel_index);
						break;

					case ConvolutionKernel::blur:
						kernel_value = convolution_kernel_blur.at(kernel_index);
						break;

					case ConvolutionKernel::emboss:
						kernel_value = convolution_kernel_emboss.at(kernel_index);
						break;

					case ConvolutionKernel::edge_detect:
						kernel_value = convolution_kernel_edge_detect.at(kernel_index);
						break;

					default:
						kernel_value = convolution_kernel_identity.at(kernel_index);
						break;
					}

					for (c = 0; c < color_component_count; ++c) {
						sum[c] = sum[c] + kernel_value * pixel_color[c];
					}
				}
			}

			for (c = 0; c < color_component_count; ++c) {
				pixel_color_new[c] = (int)ofClamp(sum[c], 0, 255);
			}

			pixel_array_new.setColor(pixel_index_new, pixel_color_new);
		}
		newImage.setFromPixels(pixel_array_new);
		image = newImage;
	}
}

// fonction d'oscillation
float Renderer::oscillate(float time, float frequency, float amplitude)
{
	return sinf(1 * 2.0f * PI / frequency) * amplitude;
}

void Renderer::setDynamicLight() {
	for (int i = 0; i < lights.size(); i++) {
		auto dynamicLightPosition = lights[i].getPosition();
		if (dynamicLightPosition.x == 0.000f && dynamicLightPosition.y == 0.000f && dynamicLightPosition.z == 0.000f) {
			shader->begin();
			lights[i] = light;
			shader->end();
			break;
		}
	}
}

void Renderer::lighting_on()
{
	if (is_active_ligh_ambient)
		ofSetGlobalAmbientColor(light_ambient);
	else
		ofSetGlobalAmbientColor(ofColor(0, 0, 0));

	if (is_active_light_directional)
		light_directional.enable();

	if (is_active_light_point)
		light_point.enable();

	if (is_active_light_spot)
		light_spot.enable();
}

// activation des lumi�res dynamiques
void Renderer::lighting_off()
{
	ofSetGlobalAmbientColor(ofColor(0, 0, 0));
	light_directional.disable();
	light_point.disable();
	light_spot.disable();
}

void Renderer::initializeSurface() {
	int width = 1024;
	int height = 768;
	surface.setup(width, height, 6, 6);

	vector<ofVec3f> vec;
	ofXml xml = ofXml();
	xml.load("surface.xml");

	auto point_parents = xml.find("/points");
	for (auto & one_parent : point_parents) {
		auto all_point_children = one_parent.getChildren("point");

		for (auto & pt : all_point_children) {

			auto x = pt.getAttribute("x").getIntValue();
			auto y = pt.getAttribute("y").getIntValue();
			auto z = pt.getAttribute("z").getIntValue();
			vec.push_back(ofVec3f(x, y, z));
		}
	}
	surface.setControlPnts(vec);
}
