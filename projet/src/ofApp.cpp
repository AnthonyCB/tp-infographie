#include "ofApp.h"

void ofApp::setup()
{
	currentCurvePoint = 0;
	ofSetWindowTitle("interface (u)");
	ofNoFill();

	renderer.setup();
	rayTracingRenderer.setup();
	camera.setup();
	index = Switch::off;
	gui.setup("interface");

	group_draw.setup("options de dessin");

	color_picker_background.set("couleur du canevas", ofColor(31), ofColor(0, 0), ofColor(255, 255));
	color_picker_background.addListener(this, &ofApp::onBackgroundRGBColorChanged);
	background_hue.set("h", (int)((ofColor(31).getHue() * 360) / color_picker_background->limit()), 0, 360);
	background_saturation.set("s", (int)((ofColor(31).getSaturation() * 100) / color_picker_background->limit()), 0, 100);
	background_brightness.set("b", (int)((ofColor(31).getBrightness() * 100) / color_picker_background->limit()), 0, 100);
	background_hue.addListener(this, &ofApp::onBackgroundHueChanged);
	background_saturation.addListener(this, &ofApp::onBackgroundSaturationChanged);
	background_brightness.addListener(this, &ofApp::onBackgroundBrightnessChanged);

	color_picker_stroke.set("couleur du trait", ofColor(255), ofColor(0, 0), ofColor(255, 255));
	color_picker_stroke.addListener(this, &ofApp::onStrokeRGBColorChanged);
	stroke_hue.set("h", (int)((ofColor(255).getHue() * 360) / color_picker_stroke->limit()), 0, 360);
	stroke_saturation.set("s", (int)((ofColor(255).getSaturation() * 100) / color_picker_stroke->limit()), 0, 100);
	stroke_brightness.set("b", (int)((ofColor(255).getBrightness() * 100) / color_picker_stroke->limit()), 0, 100);
	stroke_hue.addListener(this, &ofApp::onStrokeHueChanged);
	stroke_saturation.addListener(this, &ofApp::onStrokeSaturationChanged);
	stroke_brightness.addListener(this, &ofApp::onStrokeBrightnessChanged);

	color_picker_fill.set("couleur de remplissage", ofColor(255), ofColor(0, 0), ofColor(255, 255));
	color_picker_fill.addListener(this, &ofApp::onFillRGBColorChanged);
	fill_hue.set("h", (int)((ofColor(255).getHue() * 360) / color_picker_fill->limit()), 0, 360);
	fill_saturation.set("s", (int)((ofColor(255).getSaturation() * 100) / color_picker_fill->limit()), 0, 100);
	fill_brightness.set("b", (int)((ofColor(255).getBrightness() * 100) / color_picker_fill->limit()), 0, 100);
	fill_hue.addListener(this, &ofApp::onFillHueChanged);
	fill_saturation.addListener(this, &ofApp::onFillSaturationChanged);
	fill_brightness.addListener(this, &ofApp::onFillBrightnessChanged);

	slider_stroke_weight.set("largeur de la ligne", 4.0f, 0.0f, 10.0f);


	group_draw.add(color_picker_background);
	group_draw.add(background_hue);
	group_draw.add(background_saturation);
	group_draw.add(background_brightness);
	group_draw.add(color_picker_stroke);
	group_draw.add(stroke_hue);
	group_draw.add(stroke_saturation);
	group_draw.add(stroke_brightness);
	group_draw.add(color_picker_fill);
	group_draw.add(fill_hue);
	group_draw.add(fill_saturation);
	group_draw.add(fill_brightness);
	group_draw.add(slider_stroke_weight);

	group_draw.minimizeAll();
	group_draw.minimize();

	gui.add(&group_draw);

	group_tools.setup("outils");

	for (size_t i = 0; i < 14; i++)
	{
		toolCheckboxes.push_back(ofParameter<bool>());
		toolCheckboxes[i] = false;
		toolCheckboxes[i].addListener(this, &ofApp::toolChanged);
	}
	toolCheckboxes[0].setName("point");
	toolCheckboxes[1].setName("ligne");
	toolCheckboxes[2].setName("carre");
	toolCheckboxes[3].setName("rectangle");
	toolCheckboxes[4].setName("triangle");
	toolCheckboxes[5].setName("quadrilatere");
	toolCheckboxes[6].setName("polygone regulier");
	toolCheckboxes[7].setName("polygone irregulier");
	toolCheckboxes[8].setName("cercle");
	toolCheckboxes[9].setName("ellipse");
	toolCheckboxes[10].setName("arc");
	toolCheckboxes[11].setName("sphere");
	toolCheckboxes[12].setName("cube");
	toolCheckboxes[13].setName("courbe");
	toolCheckboxes[0] = true;
	currentTool = Tool::point;

	for (size_t i = 0; i < toolCheckboxes.size(); i++)
	{
		group_tools.add(toolCheckboxes[i]);
	}

	slider_sides_regular_poly.set("Nombre faces polygones régulier", 5, 3, 20);
	group_tools.add(slider_sides_regular_poly);

	group_tools.minimizeAll();
	group_tools.minimize();

	gui.add(&group_tools);

	exportButton.setup("exporter image application");
	exportButton.addListener(this, &ofApp::exporter);
	gui.add(&exportButton);

	boundingBoxCheckbox.setName("activer le bounding box");
	boundingBoxCheckbox = false;
	gui.add(boundingBoxCheckbox);

	checkbox.setName("visible");
	gui.add(checkbox);

	checkbox = true;

	group_filtering.setup("Filtres");

	f_identityButton.setup("Identity");
	f_identityButton.addListener(this, &ofApp::f_identityPressed);

	f_sharpenButton.setup("Sharpen");
	f_sharpenButton.addListener(this, &ofApp::f_sharpenPressed);

	f_blurButton.setup("Blur");
	f_blurButton.addListener(this, &ofApp::f_blurPressed);

	f_embossButton.setup("Emboss");
	f_embossButton.addListener(this, &ofApp::f_embossPressed);

	f_edgeDetectButton.setup("Edge dectect");
	f_edgeDetectButton.addListener(this, &ofApp::f_edgeDetectPressed);

	group_filtering.add(&f_identityButton);
	group_filtering.add(&f_sharpenButton);
	group_filtering.add(&f_blurButton);
	group_filtering.add(&f_embossButton);
	group_filtering.add(&f_edgeDetectButton);

	group_filtering.minimizeAll();
	group_filtering.minimize();

	gui.add(&group_filtering);

	groupe_mapping.setup("Mapping");
	slider_exposure.set("Exposure factor", 1.0f, 0.0f, 5.0f);
	groupe_mapping.add(slider_exposure);

	groupe_mapping.minimizeAll();
	groupe_mapping.minimize();

	gui.add(&groupe_mapping);

	shaderTextureCheckbox.setName("Activer texture procédurale");
	shaderTextureCheckbox = false;

	gui.add(shaderTextureCheckbox);


	group_light.setup("Lumieres");
	for (size_t i = 0; i < 4; i++)
	{
		lightCheckboxes.push_back(ofParameter<bool>());
		lightCheckboxes[i] = false;
		lightCheckboxes[i].addListener(this, &ofApp::lightChanged);
	}
	lightCheckboxes[0].setName("ambiante");
	lightCheckboxes[1].setName("directionnelle");
	lightCheckboxes[2].setName("ponctuelle");
	lightCheckboxes[3].setName("projecteur");
	lightCheckboxes[0] = true;
	currentLight = Light::ambiante;

	lightOnOffbox.push_back(ofParameter<bool>());
	lightOnOffbox[0] = false;
	lightOnOffbox[0].addListener(this, &ofApp::switchChanged);
	lightOnOffbox[0].setName("On/Off");
	currentSwitch = Switch::off;

	group_light.add(lightOnOffbox[0]);
	for (size_t i = 0; i < lightCheckboxes.size(); i++)
	{
		group_light.add(lightCheckboxes[i]);
	}


	slider_sides_light_intensity.set("intensite", 5, 3, 20);
	group_light.add(slider_sides_light_intensity);

	gui.add(&group_light);

	rayTracing_tools.setup("Ray Tracing");
	rayTracingTooltip.set("Ray Tracing", false);
	rayColor1.set("color sphere 1", ofColor(255.0f, 0, 0));
	rayColor2.set("color sphere 2", ofColor(0, 0, 255.0f));
	rayRadius1.set("radius sphere 1", .6, .0, 10.);
	rayRadius2.set("radius sphere 2", .85, .0, 10.);
	rayIntLight.set("intensity light", .8, .0, 5.);
	raySpeLight.set("specular light", ofVec2f(1., 50.), ofVec2f(0., 0.), ofVec2f(100., 100.));
	rayColLight.set("color light", ofColor(0, 255.0f, 0));
	rayAmbient.set("ambient light", .2, .0, 1.);

	rayPosSphere1.set("position sphere 1", ofVec3f(.85, .1, 1.), ofVec3f(-5., -5., -5.), ofVec3f(5., 5., 5.));
	rayPosSphere2.set("position sphere 2", ofVec3f(-1.3, .9, 2.25), ofVec3f(-5., -5., -5.), ofVec3f(5., 5., 5.));
	
	rayTracing_tools.add(rayTracingTooltip);
	rayTracing_tools.add(rayColor1);
	rayTracing_tools.add(rayColor2);
	rayTracing_tools.add(rayRadius1);
	rayTracing_tools.add(rayRadius2);
	rayTracing_tools.add(rayIntLight);
	rayTracing_tools.add(raySpeLight);
	rayTracing_tools.add(rayColLight);
	rayTracing_tools.add(rayAmbient);
	rayTracing_tools.add(rayPosSphere1);
	rayTracing_tools.add(rayPosSphere2);

	rayTracing_tools.minimizeAll();
	rayTracing_tools.minimize();

	gui.add(&rayTracing_tools);
		
	group_camera.setup("camera");
	cameraId.set("camera active", 0, 0, 5);
	cameraId.addListener(this, &ofApp::onCameraChanged);
	cameraName.set("camera active", "front");
	perspective.set("perspective", true);
	perspective.addListener(this, &ofApp::projectionChanged);
	group_camera.add(cameraId);
	group_camera.add(cameraName);
	group_camera.add(perspective);

	gui.add(&group_camera);

	setMouseIcons();
}

void ofApp::update()
{
	camera.is_camera_move_forward = is_key_press_w;
	camera.is_camera_move_backward = is_key_press_s;
	camera.is_camera_move_left = is_key_press_left;
	camera.is_camera_move_right = is_key_press_right;
	camera.is_camera_move_up = is_key_press_up;
	camera.is_camera_move_down = is_key_press_down;
	camera.is_camera_fov_narrow = is_key_press_plus;
	camera.is_camera_fov_wide = is_key_press_minus;

	renderer.background_color = color_picker_background;
	renderer.stroke_color = color_picker_stroke;
	renderer.stroke_weight = slider_stroke_weight;
	renderer.fill_color = color_picker_fill;

	renderer.mouseIcon = mouseIcons[currentTool];
	glm::vec2 mouseLocation(ofGetPreviousMouseX(), ofGetPreviousMouseY());
	renderer.mouseIcon.translate(getMousePosition());
	renderer.exposureFactor = slider_exposure;
	renderer.addTexture = shaderTextureCheckbox;
	camera.update();
	renderer.update();

	rayTracingRenderer.color1 = ofVec3f(rayColor1.get().r/255.0f, rayColor1.get().g / 255.0f, rayColor1.get().b / 255.0f);
	rayTracingRenderer.color2 = ofVec3f(rayColor2.get().r / 255.0f, rayColor2.get().g / 255.0f, rayColor2.get().b / 255.0f);
	rayTracingRenderer.radius1 = rayRadius1;
	rayTracingRenderer.radius2 = rayRadius2;
	rayTracingRenderer.speLight = raySpeLight;
	rayTracingRenderer.colorLight = ofVec3f(rayColLight.get().r / 255.0f, rayColLight.get().g / 255.0f, rayColLight.get().b / 255.0f);
	rayTracingRenderer.ambient = rayAmbient;
	rayTracingRenderer.intensity = rayIntLight;
	rayTracingRenderer.posSphere1 = rayPosSphere1;
	rayTracingRenderer.posSphere2 = rayPosSphere2;
}

void ofApp::draw()
{
	if (rayTracingTooltip == false) {
		camera.camera->begin();
		renderer.draw();
		camera.camera->end();		
		drawTempPrimitives();
		if (boundingBoxCheckbox) {
			renderer.drawBoundingBoxes();
		}
	}
	else {
		camera.camera->begin();
		rayTracingRenderer.draw();
		camera.camera->end();
	}

	if (checkbox)
		gui.draw();

}

glm::vec3 ofApp::getMousePosition() {
	glm::vec3 mouseLocation(ofGetPreviousMouseX(), ofGetPreviousMouseY(), 0);
	return mouseLocation;
}

void ofApp::drawTempPrimitives() {
	glm::vec2 mouseLocation(getMousePosition());
	if ((currentTool == 1 || currentTool == 4 || currentTool == 5) && drawingSteps.size() == 1) {
		ofPath line = ofPath();
		line.moveTo(drawingSteps[0].x, drawingSteps[0].y);
		line.lineTo(mouseLocation);
		line.setFillColor(color_picker_fill);
		line.setStrokeColor(color_picker_stroke);
		line.setStrokeWidth(slider_stroke_weight);
		line.draw();
	}
	else if (currentTool == 2 && drawingSteps.size() == 1) {
		ofPath rectangle = ofPath();
		float dimention = std::max(mouseLocation.x - drawingSteps[0].x, mouseLocation.y - drawingSteps[0].y);
		rectangle.rectangle(drawingSteps[0].x, drawingSteps[0].y, dimention, dimention);
		rectangle.setFillColor(color_picker_fill);
		rectangle.setStrokeColor(color_picker_stroke);
		rectangle.setStrokeWidth(slider_stroke_weight);
		rectangle.draw();
	}
	else if (currentTool == 3 && drawingSteps.size() == 1) {
		ofPath rectangle = ofPath();
		rectangle.rectangle(drawingSteps[0].x, drawingSteps[0].y, mouseLocation.x - drawingSteps[0].x, mouseLocation.y - drawingSteps[0].y);
		rectangle.setFillColor(color_picker_fill);
		rectangle.setStrokeColor(color_picker_stroke);
		rectangle.setStrokeWidth(slider_stroke_weight);
		rectangle.draw();
	}
	else if ((currentTool == 4 || currentTool == 5) && drawingSteps.size() == 2) {
		ofPath triangle = ofPath();
		triangle.triangle(drawingSteps[0].x, drawingSteps[0].y, drawingSteps[1].x, drawingSteps[1].y, mouseLocation.x, mouseLocation.y);
		triangle.setFillColor(color_picker_fill);
		triangle.setStrokeColor(color_picker_stroke);
		triangle.setStrokeWidth(slider_stroke_weight);
		triangle.draw();
	}
	else if (currentTool == 5 && drawingSteps.size() == 3) {
		ofPath quad = ofPath();
		quad.moveTo(drawingSteps[0].x, drawingSteps[0].y);
		quad.lineTo(drawingSteps[1].x, drawingSteps[1].y);
		quad.lineTo(drawingSteps[2].x, drawingSteps[2].y);
		quad.lineTo(mouseLocation);
		quad.lineTo(drawingSteps[0].x, drawingSteps[0].y);
		quad.setFilled(true);
		quad.setFillColor(color_picker_fill);
		quad.setStrokeColor(color_picker_stroke);
		quad.setStrokeWidth(slider_stroke_weight);
		quad.draw();
	}
	else if (currentTool == 6 && drawingSteps.size() == 1) {
		ofPath poly = ofPath();
		float polyAngle = 360 / slider_sides_regular_poly;
		poly.moveTo(mouseLocation.x - drawingSteps[0].x, mouseLocation.y - drawingSteps[0].y);
		for (size_t i = 0; i < slider_sides_regular_poly; i++)
		{
			poly.rotate(polyAngle, ofVec3f(0, 0, 1));
			poly.lineTo(mouseLocation.x - drawingSteps[0].x, mouseLocation.y - drawingSteps[0].y);
		}
		poly.translate(glm::vec2(drawingSteps[0].x, drawingSteps[0].y));
		poly.setFilled(true);
		poly.setFillColor(color_picker_fill);
		poly.setStrokeColor(color_picker_stroke);
		poly.setStrokeWidth(slider_stroke_weight);
		poly.draw();
	}
	else if (currentTool == 7 && drawingSteps.size() != 0) {
		ofPath poly = ofPath();
		poly.moveTo(drawingSteps[0].x, drawingSteps[0].y);
		for (size_t i = 1; i < drawingSteps.size(); i++)
		{
			poly.lineTo(drawingSteps[i].x, drawingSteps[i].y);
		}
		poly.lineTo(mouseLocation);
		poly.lineTo(drawingSteps[0].x, drawingSteps[0].y);
		poly.setFilled(true);
		poly.setFillColor(color_picker_fill);
		poly.setStrokeColor(color_picker_stroke);
		poly.setStrokeWidth(slider_stroke_weight);
		poly.draw();
	}
	else if (currentTool == 8 && drawingSteps.size() == 1) {
		ofPath circle = ofPath();
		float radious = sqrt(pow(mouseLocation.x - drawingSteps[0].x, 2) + pow(mouseLocation.y - drawingSteps[0].y, 2));
		circle.circle(drawingSteps[0].x, drawingSteps[0].y, radious);
		circle.setFillColor(color_picker_fill);
		circle.setStrokeColor(color_picker_stroke);
		circle.setStrokeWidth(slider_stroke_weight);
		circle.draw();
	}
	else if ((currentTool == 9 || currentTool == 10) && drawingSteps.size() == 1) {
		ofPath line = ofPath();
		line.moveTo(drawingSteps[0].x, drawingSteps[0].y);
		line.lineTo(mouseLocation.x, drawingSteps[0].y);
		line.lineTo(drawingSteps[0].x - (mouseLocation.x - drawingSteps[0].x), drawingSteps[0].y);
		line.setFillColor(color_picker_fill);
		line.setStrokeColor(color_picker_stroke);
		line.setStrokeWidth(slider_stroke_weight);
		line.draw();
	}
	else if ((currentTool == 9 || currentTool == 10) && drawingSteps.size() == 2) {
		ofPath ellipse = ofPath();
		float xRadious = (drawingSteps[1].x - drawingSteps[0].x) * 2;
		float yRadious = (mouseLocation.y - drawingSteps[0].y) * 2;
		ellipse.ellipse(drawingSteps[0].x, drawingSteps[0].y, xRadious, yRadious);
		ellipse.setFillColor(color_picker_fill);
		ellipse.setStrokeColor(color_picker_stroke);
		ellipse.setStrokeWidth(slider_stroke_weight);
		ellipse.draw();
	}
	else if (currentTool == 10 && drawingSteps.size() == 3) {
		ofPath arc = ofPath();
		float xRadious = abs(drawingSteps[1].x - drawingSteps[0].x);
		float yRadious = -1 * abs(drawingSteps[2].y - drawingSteps[0].y);
		float angleBegin = (atan2(mouseLocation.x - drawingSteps[0].x, mouseLocation.y - drawingSteps[0].y) - (PI / 2)) * 180 / PI;
		float angleEnd = 0;
		arc.arc(drawingSteps[0].x, drawingSteps[0].y, xRadious, yRadious, angleBegin, angleEnd);
		arc.setFillColor(color_picker_fill);
		arc.setStrokeColor(color_picker_stroke);
		arc.setStrokeWidth(slider_stroke_weight);
		arc.draw();
		ofLog() << angleBegin;
	}
	else if (currentTool == 10 && drawingSteps.size() == 4) {
		ofPath arc = ofPath();
		float xRadious = abs(drawingSteps[1].x - drawingSteps[0].x);
		float yRadious = -1 * abs(drawingSteps[2].y - drawingSteps[0].y);
		float angleBegin = (atan2(drawingSteps[3].x - drawingSteps[0].x, drawingSteps[3].y - drawingSteps[0].y) - (PI / 2)) * 180 / PI;
		float angleEnd = (atan2(mouseLocation.x - drawingSteps[0].x, mouseLocation.y - drawingSteps[0].y) - (PI / 2)) * 180 / PI;
		arc.arc(drawingSteps[0].x, drawingSteps[0].y, xRadious, yRadious, angleBegin, angleEnd);
		arc.setFillColor(color_picker_fill);
		arc.setStrokeColor(color_picker_stroke);
		arc.setStrokeWidth(slider_stroke_weight);
		arc.draw();
	}
	else if (currentTool == 11 && drawingSteps.size() == 1) {
		ofSpherePrimitive sphere = ofSpherePrimitive();
		float radious = sqrt(pow(mouseLocation.x - drawingSteps[0].x, 2) + pow(mouseLocation.y - drawingSteps[0].y, 2));
		sphere.setRadius(radious);
		sphere.setPosition(drawingSteps[0].x, drawingSteps[0].y, 0);
		sphere.drawWireframe();
	}
	else if (currentTool == 12 && drawingSteps.size() == 1) {
		ofBoxPrimitive cube = ofBoxPrimitive();
		float dimention = std::max(mouseLocation.x - drawingSteps[0].x, mouseLocation.y - drawingSteps[0].y);
		cube.set(dimention, dimention, dimention);
		cube.setPosition(drawingSteps[0].x, drawingSteps[0].y, 0);
		cube.drawWireframe();
	}
	else if (currentTool == 13 && drawingSteps.size() > 0) {
		ofPolyline curve = ofPolyline();
		std::vector<ofPath> controlPoints;
		for (size_t i = 0; i < drawingSteps.size(); i++)
		{
			if (currentCurvePoint == i) {
				curve.curveTo(ofPoint(mouseLocation));
				ofPath point = ofPath();
				point.circle(mouseLocation.x, mouseLocation.y, 10);
				point.setFillColor(ofColor(0, 255, 0));
				point.setStrokeColor(ofColor(0, 255, 0));
				controlPoints.push_back(point);
			}
			else {
				curve.curveTo(drawingSteps[i]);
				ofPath point = ofPath();
				point.circle(drawingSteps[i].x, drawingSteps[i].y, 10);
				point.setFillColor(ofColor(127));
				point.setStrokeColor(ofColor(127));;
				controlPoints.push_back(point);
			}
		}
		if (currentCurvePoint == drawingSteps.size()) {
			curve.curveTo(ofPoint(mouseLocation));
			ofPath point = ofPath();
			point.circle(mouseLocation.x, mouseLocation.y, 10);
			point.setFillColor(ofColor(0, 255, 0));
			point.setStrokeColor(ofColor(0, 255, 0));
			controlPoints.push_back(point);
		}
		curve.draw();
		for (size_t i = 0; i < controlPoints.size(); i++)
		{
			controlPoints[i].draw();
		}
	}
}

void ofApp::setMouseIcons() {

	for (size_t i = 0; i < toolCheckboxes.size(); i++)
	{
			mouseIcons.push_back(ofPath());
	}

		mouseIcons[0].circle(0, 0, 0, 1);

		mouseIcons[1].moveTo(-5, 0, 0);
		mouseIcons[1].lineTo(5, 0, 0);
		mouseIcons[1].setStrokeWidth(1);

		mouseIcons[2].rectangle(-5, -5, 5, 5);

		mouseIcons[3].rectangle(-10, -5, 10, 5);

		mouseIcons[4].triangle(-5, 5, 0, -5, 5, 5);

		mouseIcons[5].moveTo(-5, -5, 0);
		mouseIcons[5].lineTo(5, 5, 0);
		mouseIcons[5].lineTo(5, -5, 0);
		mouseIcons[5].lineTo(-5, 5, 0);
		mouseIcons[5].lineTo(-5, -5, 0);
		mouseIcons[5].setFilled(true);

		float octogonAngle = 360 / 8;
		mouseIcons[6].moveTo(-5, 0, 0);
		for (size_t i = 0; i < 8; i++)
		{
			mouseIcons[6].rotate(octogonAngle, ofVec3f(0, 0, 1));
			mouseIcons[6].lineTo(-5, 0, 0);
		}
		mouseIcons[6].setFilled(true);

		mouseIcons[7].moveTo(-5, -5, 0);
		mouseIcons[7].lineTo(0, 0, 0);
		mouseIcons[7].lineTo(5, -5, 0);
		mouseIcons[7].lineTo(5, 5, 0);
		mouseIcons[7].lineTo(-5, 5, 0);
		mouseIcons[7].lineTo(-5, -5, 0);
		mouseIcons[7].setFilled(true);

		mouseIcons[8].circle(0, 0, 0, 5);

		mouseIcons[9].ellipse(0, 0, 10, 5);

		mouseIcons[10].arc(0, 0, 5, 5, 0, 270);
}

//app events
void ofApp::windowResized(int w, int h)
{
	ofSetWindowShape(w, h);
}
void ofApp::keyPressed(int key)
{
	switch (key) 
	{
	case 43: //+
		is_key_press_plus = true;
		break;

	case 45: //-
		is_key_press_minus = true;
		break;

	case 115: //s
		is_key_press_s = true;
		break;

	case 119: //w
		is_key_press_w = true;
		break;

	case OF_KEY_LEFT:
		is_key_press_left = true;
		break;

	case OF_KEY_UP:
		is_key_press_up = true;
		break;

	case OF_KEY_RIGHT:
		is_key_press_right = true;
		break;

	case OF_KEY_DOWN:
		is_key_press_down = true;
		break;
	case 49: // touche 1
		renderer.shader_active = ShaderType::color_fill;
		break;

	case 50: // touche 2
		renderer.shader_active = ShaderType::lambert;
		break;

	case 51: // touche 3
		renderer.shader_active = ShaderType::gouraud;
		break;

	case 52: // touche 4
		renderer.shader_active = ShaderType::phong;
		break;

	case 53: // touche 5
		renderer.shader_active = ShaderType::blinn_phong;
		break;
	default:
		break;
	}
}
void ofApp::keyReleased(int key)
{
	switch (key)
	{
	case 43: //+
		is_key_press_plus = false;
		break;

	case 45: //-
		is_key_press_minus = false;
		break;

	case 114: //r
		camera.reset();
		break;

	case 115: //s
		is_key_press_s = false;
		break;

	case 117: //u
		if (drawingSteps.size() == 0)
		{
			checkbox = !checkbox;
		}
		break;

	case 119: //w
		is_key_press_w = false;
		break;

	case OF_KEY_LEFT:
		is_key_press_left = false;
		break;

	case OF_KEY_UP:
		is_key_press_up = false;
		break;

	case OF_KEY_RIGHT:
		is_key_press_right = false;
		break;

	case OF_KEY_DOWN:
		is_key_press_down = false;
		break;

	case ' ':
		exporter();
		break;

	default:
		break;
	}
	if (currentTool == Tool::curve && drawingSteps.size() > 0) {
		if (key == 'a') {
			if (currentCurvePoint == 0) {
				currentCurvePoint = drawingSteps.size();
			}
			else {
				currentCurvePoint--;
			}
		}
		else if (key == 'd') {
			if (currentCurvePoint == drawingSteps.size()) {
				currentCurvePoint = 0;
			}
			else {
				currentCurvePoint++;
			}
		}
		else if (key == OF_KEY_RETURN && drawingSteps.size() >= 4)
		{
			ofPolyline curve = ofPolyline();
			for (size_t i = 0; i < drawingSteps.size(); i++)
			{
				curve.curveTo(drawingSteps[i]);
			}
			if (currentCurvePoint == drawingSteps.size()) {
				curve.curveTo(ofPoint(getMousePosition()));
			}
			renderer.curves.push_back(curve);
			drawingSteps.clear();
			currentCurvePoint = 0;
			checkbox = true;
		}
	}
}
void ofApp::mousePressed(int x, int y, int button) {
	glm::vec3 mouseLocation(getMousePosition());
	x = mouseLocation.x;
	y = mouseLocation.y;
	if (button == 0) {
		if (index == 0) {
			if (currentSwitch == 0) {
					renderer.setDynamicLight();
			}
		}
		else {
			if (currentTool == 0) {
				ofPath point = ofPath();
				point.circle(x, y, 1);
				point.setFillColor(color_picker_fill);
				point.setStrokeColor(color_picker_stroke);
				point.setStrokeWidth(slider_stroke_weight);
				renderer.shapes.push_back(point);
			}
			else if (currentTool == 1) {
				checkbox = false;
				if (drawingSteps.size() == 0) {
					drawingSteps.push_back(glm::vec2(x, y));
				}
				else {
					ofPath line = ofPath();
					line.moveTo(drawingSteps[0].x, drawingSteps[0].y);
					line.lineTo(x, y);
					line.setFillColor(color_picker_fill);
					line.setStrokeColor(color_picker_stroke);
					line.setStrokeWidth(slider_stroke_weight);
					renderer.shapes.push_back(line);
					drawingSteps.clear();
					checkbox = true;
				}
			}
			else if (currentTool == 2) {
				checkbox = false;
				if (drawingSteps.size() == 0) {
					drawingSteps.push_back(glm::vec2(x, y));
				}
				else {
					ofPath rectangle = ofPath();
					float dimention = std::max(x - drawingSteps[0].x, y - drawingSteps[0].y);
					rectangle.rectangle(drawingSteps[0].x, drawingSteps[0].y, dimention, dimention);
					rectangle.setFillColor(color_picker_fill);
					rectangle.setStrokeColor(color_picker_stroke);
					rectangle.setStrokeWidth(slider_stroke_weight);
					renderer.shapes.push_back(rectangle);
					drawingSteps.clear();
					checkbox = true;
				}
			}
			else if (currentTool == 3) {
				checkbox = false;
				if (drawingSteps.size() == 0) {
					drawingSteps.push_back(glm::vec2(x, y));
				}
				else {
					ofPath rectangle = ofPath();
					rectangle.rectangle(drawingSteps[0].x, drawingSteps[0].y, x - drawingSteps[0].x, y - drawingSteps[0].y);
					rectangle.setFillColor(color_picker_fill);
					rectangle.setStrokeColor(color_picker_stroke);
					rectangle.setStrokeWidth(slider_stroke_weight);
					renderer.shapes.push_back(rectangle);
					drawingSteps.clear();
					checkbox = true;
				}
			}
			else if (currentTool == 4) {
				checkbox = false;
				if (drawingSteps.size() <= 1) {
					drawingSteps.push_back(glm::vec2(x, y));
				}
				else {
					ofPath triangle = ofPath();
					triangle.triangle(drawingSteps[0].x, drawingSteps[0].y, drawingSteps[1].x, drawingSteps[1].y, x, y);
					triangle.setFillColor(color_picker_fill);
					triangle.setStrokeColor(color_picker_stroke);
					triangle.setStrokeWidth(slider_stroke_weight);
					renderer.shapes.push_back(triangle);
					drawingSteps.clear();
					checkbox = true;
				}
			}
			else if (currentTool == 5) {
				checkbox = false;
				if (drawingSteps.size() <= 2) {
					drawingSteps.push_back(glm::vec2(x, y));
				}
				else {
					ofPath quad = ofPath();
					quad.moveTo(drawingSteps[0].x, drawingSteps[0].y);
					quad.lineTo(drawingSteps[1].x, drawingSteps[1].y);
					quad.lineTo(drawingSteps[2].x, drawingSteps[2].y);
					quad.lineTo(x, y);
					quad.lineTo(drawingSteps[0].x, drawingSteps[0].y);
					quad.setFilled(true);
					quad.setFillColor(color_picker_fill);
					quad.setStrokeColor(color_picker_stroke);
					quad.setStrokeWidth(slider_stroke_weight);
					renderer.shapes.push_back(quad);
					drawingSteps.clear();
					checkbox = true;
				}
			}
			else if (currentTool == 6) {
				checkbox = false;
				if (drawingSteps.size() == 0) {
					drawingSteps.push_back(glm::vec2(x, y));
				}
				else {
					ofPath poly = ofPath();
					float polyAngle = 360 / slider_sides_regular_poly;
					poly.moveTo(x - drawingSteps[0].x, y - drawingSteps[0].y);
					for (size_t i = 0; i < slider_sides_regular_poly; i++)
					{
						poly.rotate(polyAngle, ofVec3f(0, 0, 1));
						poly.lineTo(x - drawingSteps[0].x, y - drawingSteps[0].y);
					}
					poly.translate(glm::vec2(drawingSteps[0].x, drawingSteps[0].y));
					poly.setFilled(true);
					poly.setFillColor(color_picker_fill);
					poly.setStrokeColor(color_picker_stroke);
					poly.setStrokeWidth(slider_stroke_weight);
					renderer.shapes.push_back(poly);
					drawingSteps.clear();
					checkbox = true;
				}
			}
			else if (currentTool == 7) {
				checkbox = false;
				if (drawingSteps.size() == 0) {
					drawingSteps.push_back(glm::vec2(x, y));
				}
				else if (drawingSteps[0].x + slider_stroke_weight >= x && drawingSteps[0].x - slider_stroke_weight <= x
					&& drawingSteps[0].y + slider_stroke_weight >= y && drawingSteps[0].y - slider_stroke_weight <= y) {
					ofPath poly = ofPath();
					poly.moveTo(drawingSteps[0].x, drawingSteps[0].y);
					for (size_t i = 1; i < drawingSteps.size(); i++)
					{
						poly.lineTo(drawingSteps[i].x, drawingSteps[i].y);
					}
					poly.lineTo(x, y);
					poly.lineTo(drawingSteps[0].x, drawingSteps[0].y);
					poly.setFilled(true);
					poly.setFillColor(color_picker_fill);
					poly.setStrokeColor(color_picker_stroke);
					poly.setStrokeWidth(slider_stroke_weight);
					renderer.shapes.push_back(poly);
					drawingSteps.clear();
					checkbox = true;
				}
				else {
					drawingSteps.push_back(glm::vec2(x, y));
				}
			}
			else if (currentTool == 8) {
				checkbox = false;
				if (drawingSteps.size() == 0) {
					drawingSteps.push_back(glm::vec2(x, y));
				}
				else {
					ofPath circle = ofPath();
					float radious = sqrt(pow(x - drawingSteps[0].x, 2) + pow(y - drawingSteps[0].y, 2));
					circle.circle(drawingSteps[0].x, drawingSteps[0].y, radious);
					circle.setFillColor(color_picker_fill);
					circle.setStrokeColor(color_picker_stroke);
					circle.setStrokeWidth(slider_stroke_weight);
					renderer.shapes.push_back(circle);
					drawingSteps.clear();
					checkbox = true;
				}
			}
			else if (currentTool == 9) {
				checkbox = false;
				if (drawingSteps.size() <= 1) {
					drawingSteps.push_back(glm::vec2(x, y));
				}
				else {
					ofPath ellipse = ofPath();
					float xRadious = (drawingSteps[1].x - drawingSteps[0].x) * 2;
					float yRadious = (y - drawingSteps[0].y) * 2;
					ellipse.ellipse(drawingSteps[0].x, drawingSteps[0].y, xRadious, yRadious);
					ellipse.setFillColor(color_picker_fill);
					ellipse.setStrokeColor(color_picker_stroke);
					ellipse.setStrokeWidth(slider_stroke_weight);
					renderer.shapes.push_back(ellipse);
					drawingSteps.clear();
					checkbox = true;
				}
			}
			else if (currentTool == 10) {
				checkbox = false;
				if (drawingSteps.size() <= 3) {
					drawingSteps.push_back(glm::vec2(x, y));
				}
				else {
					ofPath arc = ofPath();
					float xRadious = abs(drawingSteps[1].x - drawingSteps[0].x);
					float yRadious = -1 * abs(drawingSteps[2].y - drawingSteps[0].y);
					float angleBegin = (atan2(drawingSteps[3].x - drawingSteps[0].x, drawingSteps[3].y - drawingSteps[0].y) - (PI / 2)) * 180 / PI;
					float angleEnd = (atan2(x - drawingSteps[0].x, y - drawingSteps[0].y) - (PI / 2)) * 180 / PI;
					arc.arc(drawingSteps[0].x, drawingSteps[0].y, xRadious, yRadious, angleBegin, angleEnd);
					arc.setFillColor(color_picker_fill);
					arc.setStrokeColor(color_picker_stroke);
					arc.setStrokeWidth(slider_stroke_weight);
					renderer.shapes.push_back(arc);
					drawingSteps.clear();
					checkbox = true;
				}
			}
			else if (currentTool == 11) {
				checkbox = false;
				if (drawingSteps.size() == 0) {
					drawingSteps.push_back(glm::vec2(x, y));
				}
				else {
					ofSpherePrimitive sphere = ofSpherePrimitive();
					float radious = sqrt(pow(x - drawingSteps[0].x, 2) + pow(y - drawingSteps[0].y, 2));
					sphere.setRadius(radious);
					sphere.setPosition(drawingSteps[0].x, drawingSteps[0].y, 0);
					renderer.shapes3D.push_back(sphere);
					drawingSteps.clear();
					checkbox = true;
				}
			}
			else if (currentTool == 12) {
				checkbox = false;
				if (drawingSteps.size() == 0) {
					drawingSteps.push_back(glm::vec2(x, y));
				}
				else {
					ofBoxPrimitive cube = ofBoxPrimitive();
					float dimention = std::max(x - drawingSteps[0].x, y - drawingSteps[0].y);
					cube.set(dimention, dimention, dimention);
					cube.setPosition(drawingSteps[0].x, drawingSteps[0].y, 0);
					cube.rotate(90.0f, 1.0f, 0.0f, 0.0f);
					renderer.shapes3D.push_back(cube);
					drawingSteps.clear();
					checkbox = true;
				}
			}
			else if (currentTool == 13) {
				checkbox = false;
				if (currentCurvePoint == drawingSteps.size()) {
					drawingSteps.push_back(glm::vec2(x, y));
					currentCurvePoint++;
				}
				else {
					drawingSteps[currentCurvePoint] = ofPoint(glm::vec2(x, y));
					currentCurvePoint++;
				}
				//to keypress for final
			}
		}
	}
}
void ofApp::dragEvent(ofDragInfo dragInfo)
{
	for (size_t i = 0; i < dragInfo.files.size(); i++)
	{
		renderer.setImageRatio(renderer.image.getWidth() / renderer.image.getHeight());
		std::string extention = dragInfo.files[i].substr(dragInfo.files[i].length() - 4, 4);
		if (extention == ".obj" || extention == ".3ds" || extention == ".smd") {
			ofxAssimpModelLoader model = ofxAssimpModelLoader();
			model.loadModel(dragInfo.files[i], true);
			model.setPosition(dragInfo.position.x, dragInfo.position.y, 0);
			renderer.models.push_back(model);
		}
		else if (extention == ".png" || extention == ".jpg") {
			ofImage image;
			image.load(dragInfo.files.at(i));
			importedImages.push_back(image);
			renderer.image = image;
		}
	}
}

void ofApp::projectionChanged(bool& value) {

	camera.is_camera_perspective = value;
	camera.setPerspective();
}

//gui events
void ofApp::toolChanged(bool& value)
{
	index = Switch::off;
	if (toolCheckboxes[currentTool] == false) {
		toolCheckboxes[currentTool] = true;
	}
	else {
		for (int i = 0; i < toolCheckboxes.size(); i++)
		{
			if (i != currentTool && toolCheckboxes[i]) {
				int oldTool = currentTool;
				currentTool = static_cast<Tool>(i);;
				toolCheckboxes[oldTool] = false;
			}
		}
	}
}
void ofApp::onCameraChanged(int& value)
{
	TypeCamera cameraActive = (TypeCamera)value;
}
void ofApp::lightChanged(bool& value)
{
	index = Switch::on;
	if (lightCheckboxes[currentLight] == false) {
		lightCheckboxes[currentLight] = true;
	}
	else {
		for (int i = 0; i < lightCheckboxes.size(); i++)
		{
			if (i != currentLight && lightCheckboxes[i]) {
				int oldLight = currentLight;
				currentLight = static_cast<Light>(i);;
				lightCheckboxes[oldLight] = false;
			}
		}
	}
	if (currentSwitch == 0) {
		renderer.is_active_light_directional = false;
		renderer.is_active_light_point = false;
		renderer.is_active_light_spot = false;
		renderer.is_active_ligh_ambient = false;
		if (currentLight == 0) {
			renderer.is_active_ligh_ambient = true;
		}
		else if (currentLight == 1) {
			renderer.is_active_light_directional = true;
		}
		else if (currentLight == 2) {
			renderer.is_active_light_point = true;
		}
		else if (currentLight == 3) {
			renderer.is_active_light_spot = true;
		}
	}
}

void ofApp::switchChanged(bool& value)
{
	index = Switch::on;
	if(lightOnOffbox[0]){
		currentSwitch = Switch::on;
		renderer.isLightActive = true;
	}
	else {
		currentSwitch = Switch::off;
		renderer.isLightActive = false;
	}
}

void ofApp::onBackgroundRGBColorChanged(ofColor& color)
{
	background_hue.removeListener(this, &ofApp::onBackgroundHueChanged);
	background_saturation.removeListener(this, &ofApp::onBackgroundSaturationChanged);
	background_brightness.removeListener(this, &ofApp::onBackgroundBrightnessChanged);
	background_hue.set((color.getHue() * 360) / color.limit());
	background_saturation.set((color.getSaturation() * 100) / color.limit());
	background_brightness.set((color.getBrightness() * 100) / color.limit());
	background_hue.addListener(this, &ofApp::onBackgroundHueChanged);
	background_saturation.addListener(this, &ofApp::onBackgroundSaturationChanged);
	background_brightness.addListener(this, &ofApp::onBackgroundBrightnessChanged);
}
void ofApp::onBackgroundHueChanged(int& value) {
	color_picker_background.removeListener(this, &ofApp::onBackgroundRGBColorChanged);
	ofColor newColor;
	newColor.setHsb((background_hue * newColor.limit()) / 360, (background_saturation * newColor.limit()) / 100, (background_brightness * newColor.limit()) / 100);
	color_picker_background = newColor;
	color_picker_background.addListener(this, &ofApp::onBackgroundRGBColorChanged);
}
void ofApp::onBackgroundSaturationChanged(int& value) {
	color_picker_background.removeListener(this, &ofApp::onBackgroundRGBColorChanged);
	ofColor newColor;
	newColor.setHsb((background_hue * newColor.limit()) / 360, (background_saturation * newColor.limit()) / 100, (background_brightness * newColor.limit()) / 100);
	color_picker_background = newColor;
	color_picker_background.addListener(this, &ofApp::onBackgroundRGBColorChanged);
}
void ofApp::onBackgroundBrightnessChanged(int& value) {
	color_picker_background.removeListener(this, &ofApp::onBackgroundRGBColorChanged);
	ofColor newColor;
	newColor.setHsb((background_hue * newColor.limit()) / 360, (background_saturation * newColor.limit()) / 100, (background_brightness * newColor.limit()) / 100);
	color_picker_background = newColor;
	color_picker_background.addListener(this, &ofApp::onBackgroundRGBColorChanged);
}
void ofApp::onStrokeRGBColorChanged(ofColor& color)
{
	stroke_hue.removeListener(this, &ofApp::onStrokeHueChanged);
	stroke_saturation.removeListener(this, &ofApp::onStrokeSaturationChanged);
	stroke_brightness.removeListener(this, &ofApp::onStrokeBrightnessChanged);
	stroke_hue.set((color.getHue() * 360) / color.limit());
	stroke_saturation.set((color.getSaturation() * 100) / color.limit());
	stroke_brightness.set((color.getBrightness() * 100) / color.limit());
	stroke_hue.addListener(this, &ofApp::onStrokeHueChanged);
	stroke_saturation.addListener(this, &ofApp::onStrokeSaturationChanged);
	stroke_brightness.addListener(this, &ofApp::onStrokeBrightnessChanged);
}
void ofApp::onStrokeHueChanged(int& value) {
	color_picker_stroke.removeListener(this, &ofApp::onStrokeRGBColorChanged);
	ofColor newColor;
	newColor.setHsb((stroke_hue * newColor.limit()) / 360, (stroke_saturation * newColor.limit()) / 100, (stroke_brightness * newColor.limit()) / 100);
	color_picker_stroke = newColor;
	color_picker_stroke.addListener(this, &ofApp::onStrokeRGBColorChanged);
}
void ofApp::onStrokeSaturationChanged(int& value) {
	color_picker_stroke.removeListener(this, &ofApp::onStrokeRGBColorChanged);
	ofColor newColor;
	newColor.setHsb((stroke_hue * newColor.limit()) / 360, (stroke_saturation * newColor.limit()) / 100, (stroke_brightness * newColor.limit()) / 100);
	color_picker_stroke = newColor;
	color_picker_stroke.addListener(this, &ofApp::onStrokeRGBColorChanged);
}
void ofApp::onStrokeBrightnessChanged(int& value) {
	color_picker_stroke.removeListener(this, &ofApp::onStrokeRGBColorChanged);
	ofColor newColor;
	newColor.setHsb((stroke_hue * newColor.limit()) / 360, (stroke_saturation * newColor.limit()) / 100, (stroke_brightness * newColor.limit()) / 100);
	color_picker_stroke = newColor;
	color_picker_stroke.addListener(this, &ofApp::onStrokeRGBColorChanged);
}
void ofApp::onFillRGBColorChanged(ofColor& color)
{
	fill_hue.removeListener(this, &ofApp::onFillHueChanged);
	fill_saturation.removeListener(this, &ofApp::onFillSaturationChanged);
	fill_brightness.removeListener(this, &ofApp::onFillBrightnessChanged);
	fill_hue.set((color.getHue() * 360) / color.limit());
	fill_saturation.set((color.getSaturation() * 100) / color.limit());
	fill_brightness.set((color.getBrightness() * 100) / color.limit());
	fill_hue.addListener(this, &ofApp::onFillHueChanged);
	fill_saturation.addListener(this, &ofApp::onFillSaturationChanged);
	fill_brightness.addListener(this, &ofApp::onFillBrightnessChanged);
}
void ofApp::onFillHueChanged(int& value) {
	color_picker_fill.removeListener(this, &ofApp::onFillRGBColorChanged);
	ofColor newColor;
	newColor.setHsb((fill_hue * newColor.limit()) / 360, (fill_saturation * newColor.limit()) / 100, (fill_brightness * newColor.limit()) / 100);
	color_picker_fill = newColor;
	color_picker_fill.addListener(this, &ofApp::onFillRGBColorChanged);
}
void ofApp::onFillSaturationChanged(int& value) {
	color_picker_fill.removeListener(this, &ofApp::onFillRGBColorChanged);
	ofColor newColor;
	newColor.setHsb((fill_hue * newColor.limit()) / 360, (fill_saturation * newColor.limit()) / 100, (fill_brightness * newColor.limit()) / 100);
	color_picker_fill = newColor;
	color_picker_fill.addListener(this, &ofApp::onFillRGBColorChanged);
}
void ofApp::onFillBrightnessChanged(int& value) {
	color_picker_fill.removeListener(this, &ofApp::onFillRGBColorChanged);
	ofColor newColor;
	newColor.setHsb((fill_hue * newColor.limit()) / 360, (fill_saturation * newColor.limit()) / 100, (fill_brightness * newColor.limit()) / 100);
	color_picker_fill = newColor;
	color_picker_fill.addListener(this, &ofApp::onFillRGBColorChanged);
}

void ofApp::f_identityPressed() {
	renderer.filtrageConvolution(ConvolutionKernel::identity);
}
void ofApp::f_sharpenPressed() {
	renderer.filtrageConvolution(ConvolutionKernel::sharpen);
}
void ofApp::f_blurPressed() {
	renderer.filtrageConvolution(ConvolutionKernel::blur);
}
void ofApp::f_embossPressed() {
	renderer.filtrageConvolution(ConvolutionKernel::emboss);
}
void ofApp::f_edgeDetectPressed() {
	renderer.filtrageConvolution(ConvolutionKernel::edge_detect);
}
void ofApp::exporter() {
	renderer.image_export("render", "jpg");
}
void ofApp::exit()
{
	for (size_t i = 0; i < toolCheckboxes.size(); i++)
	{
		toolCheckboxes[i].removeListener(this, &ofApp::toolChanged);
	}

	ofLog() << "<app::exit>";
}