#version 330

uniform float width;
uniform float height;
out vec4 outputColor;

void main()
{
    float r = gl_FragCoord.x / 500;
    float g = gl_FragCoord.y / 800;
    float b = 1.0;
    float a = 1.0;
    outputColor = vec4(r, g, b, a);
}