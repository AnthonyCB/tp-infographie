#version 330


// attributs uniformes
uniform sampler2D image;
uniform float factor;

// attribut en entrée
in vec2 surface_texcoord;

// attribut en sortie
out vec4 exposedColor;

void main()
{
  // échantillonner la texture
  vec3 texture_sample = texture(image, surface_texcoord).rgb;

  vec3 mapped = vec3(1.0) - exp(-texture_sample * factor);

  // couleur finale du fragment
  exposedColor = vec4(mapped, 1.0);
}
