// IFT3100H19 ~ blinn_phong_330_fs.glsl

#version 330

// attributs interpolés à partir des valeurs en sortie du shader de sommet
in vec3 surface_position;
in vec3 surface_normal;

// attribut en sortie
out vec4 fragment_color;
out vec4 pointLights1_color;
out vec4 pointLights2_color;
out vec4 pointLights3_color;
out vec4 pointLights4_color;

// couleurs de réflexion du matériau
uniform vec3 color_ambient;
uniform vec3 color_diffuse;
uniform vec3 color_specular;

uniform vec3 pointLights1Color_ambient;
uniform vec3 pointLights1Color_diffuse;
uniform vec3 pointLights1Color_specular;


// facteur de brillance spéculaire du matériau
uniform float brightness;
uniform float pointLights1Brightness;

// position d'une source de lumière
uniform vec3 light_position;
uniform vec3 pointLights1Light_position;
uniform vec3 pointLights2Light_position;
uniform vec3 pointLights3Light_position;
uniform vec3 pointLights4Light_position;

void main()
{
  // re-normaliser la normale après interpolation
  vec3 n = normalize(surface_normal);

  // calculer la direction de la surface vers la lumière (l)
  vec3 l = normalize(light_position - surface_position);
  vec3 l2 = normalize(pointLights1Light_position - surface_position);
  vec3 l3 = normalize(pointLights2Light_position - surface_position);
  vec3 l4 = normalize(pointLights3Light_position - surface_position);
  vec3 l5 = normalize(pointLights4Light_position - surface_position);

  // calculer le niveau de réflexion diffuse (n • l)
  float reflection_diffuse = max(dot(n, l), 0.0);
  float reflection_diffuse2 = max(dot(n, l2), 0.0);
  float reflection_diffuse3 = max(dot(n, l3), 0.0);
  float reflection_diffuse4 = max(dot(n, l4), 0.0);
  float reflection_diffuse5 = max(dot(n, l5), 0.0);

  // réflexion spéculaire par défaut
  float reflection_specular = 0.0;

  // calculer la réflexion spéculaire seulement s'il y a réflexion diffuse
  if (reflection_diffuse > 0.0)
  {
    // calculer la direction de la surface vers la caméra (v)
    vec3 v = normalize(-surface_position);

    // calculer la direction du demi-vecteur de réflection (h) en fonction du vecteur de vue (v) et de lumière (l)
    vec3 h = normalize(v + l);

    // calculer le niveau de réflexion spéculaire (n • h)
    reflection_specular = pow(max(dot(n, h), 0.0), brightness);
  }
  
  float reflection_specular2 = 0.0;
  
    if (reflection_diffuse2 > 0.0)
  {
    // calculer la direction de la surface vers la caméra (v)
    vec3 v2 = normalize(-surface_position);

    // calculer la direction du demi-vecteur de réflection (h) en fonction du vecteur de vue (v) et de lumière (l)
    vec3 h2 = normalize(v2 + l2);

    // calculer le niveau de réflexion spéculaire (n • h)
    reflection_specular2 = pow(max(dot(n, h2), 0.0), brightness);
  }
  
    float reflection_specular3= 0.0;
  
    if (reflection_diffuse3 > 0.0)
  {
    // calculer la direction de la surface vers la caméra (v)
    vec3 v3 = normalize(-surface_position);

    // calculer la direction du demi-vecteur de réflection (h) en fonction du vecteur de vue (v) et de lumière (l)
    vec3 h3 = normalize(v3 + l3);

    // calculer le niveau de réflexion spéculaire (n • h)
    reflection_specular3 = pow(max(dot(n, h3), 0.0), brightness);
  }
  
    float reflection_specular4= 0.0;
  
    if (reflection_diffuse4 > 0.0)
  {
    // calculer la direction de la surface vers la caméra (v)
    vec3 v4 = normalize(-surface_position);

    // calculer la direction du demi-vecteur de réflection (h) en fonction du vecteur de vue (v) et de lumière (l)
    vec3 h4 = normalize(v4 + l4);

    // calculer le niveau de réflexion spéculaire (n • h)
    reflection_specular4 = pow(max(dot(n, h4), 0.0), brightness);
  }
  
    float reflection_specular5= 0.0;
  
    if (reflection_diffuse5 > 0.0)
  {
    // calculer la direction de la surface vers la caméra (v)
    vec3 v5 = normalize(-surface_position);

    // calculer la direction du demi-vecteur de réflection (h) en fonction du vecteur de vue (v) et de lumière (l)
    vec3 h5 = normalize(v5 + l5);

    // calculer le niveau de réflexion spéculaire (n • h)
    reflection_specular5 = pow(max(dot(n, h5), 0.0), brightness);
  }

  // calculer la couleur du fragment
  fragment_color = vec4(
    color_ambient +
    color_diffuse * reflection_diffuse +
    color_specular * reflection_specular, 1.0);

  pointLights1_color = vec4(
    pointLights1Color_ambient +
    pointLights1Color_diffuse * reflection_diffuse2 +
    pointLights1Color_specular * reflection_specular2, 1.0);	
	
  pointLights2_color = vec4(
    color_ambient +
    color_diffuse * reflection_diffuse3 +
    color_specular * reflection_specular3, 1.0);
	
  pointLights3_color = vec4(
    color_ambient +
    color_diffuse * reflection_diffuse4 +
    color_specular * reflection_specular4, 1.0);	
	
  pointLights4_color = vec4(
    color_ambient +
    color_diffuse * reflection_diffuse5 +
    color_specular * reflection_specular5, 1.0);	
}
